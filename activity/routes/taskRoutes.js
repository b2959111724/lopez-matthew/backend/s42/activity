
const express = require('express');
const router = express.Router();

const taskController = require('../controllers/taskController');

// Route for getting all tasks
router.get('/', taskController.getAllTasks);

// Route for getting a specific task
router.get('/:id', taskController.getTaskById);

// Route for changing the status of a task to "complete"
router.put('/:id/complete', taskController.completeTask);

// Route for updating a task
router.put('/:id', taskController.updateTask);

// Route for creating a new task
router.post('/', taskController.createTask);

// Route for deleting a task
router.delete('/:id', taskController.deleteTask);

module.exports = router;
