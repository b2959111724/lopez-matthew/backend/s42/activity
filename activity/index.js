
const express = require('express');
const app = express();
app.use(express.json());

const taskRoutes = require('./routes/taskRoutes');

// Use the taskRoutes for handling /tasks related endpoints
app.use('/tasks', taskRoutes);

// Start the server
const port = 3000; // Replace this with the desired port number
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
