
let tasks = [
  { id: 1, title: 'Task 1', status: 'incomplete' },
  { id: 2, title: 'Task 2', status: 'incomplete' },
  { id: 3, title: 'Task 3', status: 'incomplete' },
  { id: 4, title: 'Task 4', status: 'incomplete' },
  { id: 5, title: 'Task 5', status: 'incomplete' }
  // Additional tasks...
];

// Controller function for retrieving a specific task
const getTaskById = (req, res) => {
  const taskId = parseInt(req.params.id);
  const task = tasks.find((t) => t.id === taskId);
  if (!task) {
    return res.status(404).json({ message: 'Task not found' });
  }
  res.json(task);
};

// Controller function for changing the status of a task to "complete"
const completeTask = (req, res) => {
  const taskId = parseInt(req.params.id);
  const task = tasks.find((t) => t.id === taskId);
  if (!task) {
    return res.status(404).json({ message: 'Task not found' });
  }
  task.status = 'complete';
  res.json(task);
};

// Controller function for creating a new task
const createTask = (req, res) => {
  const { title } = req.body;
  if (!title) {
    return res.status(400).json({ message: 'Title is required' });
  }
  const newTask = { id: tasks.length + 1, title, status: 'incomplete' };
  tasks.push(newTask);
  res.status(201).json(newTask);
};

// Controller function for updating a task
const updateTask = (req, res) => {
  const taskId = parseInt(req.params.id);
  const task = tasks.find((t) => t.id === taskId);
  if (!task) {
    return res.status(404).json({ message: 'Task not found' });
  }

  const { title, status } = req.body;
  if (title) {
    task.title = title;
  }
  if (status) {
    task.status = status;
  }

  res.json(task);
};

// Controller function for deleting a task
const deleteTask = (req, res) => {
  const taskId = parseInt(req.params.id);
  const index = tasks.findIndex((t) => t.id === taskId);
  if (index === -1) {
    return res.status(404).json({ message: 'Task not found' });
  }

  const deletedTask = tasks.splice(index, 1);
  res.json(deletedTask[0]);
};

// Controller function for getting all tasks
const getAllTasks = (req, res) => {
  console.log(tasks)
  res.json(tasks);
};
module.exports = { getTaskById, completeTask, createTask, updateTask, deleteTask, getAllTasks };